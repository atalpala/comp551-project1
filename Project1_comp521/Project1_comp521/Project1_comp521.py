import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import random

from sklearn.model_selection import train_test_split 
from sklearn.linear_model import LinearRegression
from sklearn import preprocessing
#from sklearn.model_selection import ShuffleSplit

#Note the dataset is filtered by participants that run more than once


def scalling_function (rank, max_value):
    value = (rank - 1)/(max_value -1)
    return value

def normalize_rank(dataset):
    print('Normalizing Rank values')
    list_years=[]

    for year in np.array(dataset['Year']):
        if year not in list_years: list_years.append(year)

    #creating a new column for the normalized Rank
    dataset ["Rank_nor"] = 0
       
    for year in list_years:
        #filter the data in teh dataFrame by the year
        scaller = max(np.array(dataset [dataset['Year']==year ] ['Rank']))
        #extract the index location of the above array
        index_items = np.array(dataset [dataset['Year']==year ].index)

        #apply the scalling_function to each location in the Column "Rank_nor"
        for index in (index_items):
            dataset.iloc[index,-1] = scalling_function(dataset.iloc[index]['Rank'], scaller)

    return dataset

def normalize_other_fields (dataset):
    print('Normalizing Age Category, Pace and Time')

    #-- use the builted fuction to normalize other fields
    min_max_scaler = preprocessing.MinMaxScaler()
    np_scaled = min_max_scaler.fit_transform( dataset [['Age Category','Pace','Time']],1)
    df_normalized = pd.DataFrame(np_scaled)
    df_normalized.columns=['Age','Pace_nor','Time_nor']
    df_normalized[['Id','Name','Sex','Rank_nor','Year','Gender_code','participations']] = dataset[['Id','Name','Sex','Rank_nor','Year','Gender_code','participations']]#,'Time'

    dataset = df_normalized
    return dataset

    #---adjust the dataset accordingly to be used in the Linear Regression
def new_DataSet (dataset):
    print('Making the new Database')
    index1 = list( dataset['Id'])
    index_list = []

    for x in index1:
        if(x not in index_list):
            index_list.append(x)

    auxiliar_dict = {}

    for x in index_list[:]:
        templist= []
        #[data['Id']==x] -> boolean DataFrame that returns true for all the rows corresponding for a unique id.
    
        #extract the name
        templist.append((np.array(dataset [dataset['Id']==x ] ['Name'] ))[0])

        #extract the highest age value
        templist.append(max(np.array(dataset [dataset['Id']==x ] ['Age'] )))

        #extract the gender
        templist.append((np.array(dataset [dataset['Id']==x ] ['Gender_code'] ))[0])

        #extract How many times each person participate
        number_of_participations= max (np.array(dataset [dataset['Id']==x ] ['participations'] ))
        templist.append(number_of_participations)
    
        #average rank
        tempy_dataFrame_piece = dataset [dataset['Id']==x ] ['Rank_nor']
        templist.append (np.mean(tempy_dataFrame_piece[:-1]))

        #t-1 rank
        templist.append (np.mean(tempy_dataFrame_piece[-2:-1]))

        #extracting fastest. pace information
        tempy_dataFrame_piece = dataset[ dataset['Id']==x] ['Pace_nor']
        templist.append(np.mean(tempy_dataFrame_piece[:-1]))

        #extracting fastest. pace information
        templist.append(min(tempy_dataFrame_piece[:-1]))

        #extracting slowest. pace information
        templist.append(max((tempy_dataFrame_piece[:-1])))

        #extracting t-1 pace information
        templist.append(max(tempy_dataFrame_piece[-2:-1]))

        #extracting future time information
        templist.append(np.array( dataset[ dataset['Id']==x] ['Time_nor'])[-1])

        #creates a dictionary of lists, in which the ID is the dictionaries' key
        auxiliar_dict[x] = templist

    final_dataset = pd.DataFrame.from_dict(auxiliar_dict, orient = 'index')
    final_dataset.columns = ['Name','Age_nor','Gender','participations','Av_Rank_nor','t_1_Rank_nor','Av_Pace_nor','Fastest_Pace_nor','Slowest_Pace_nor','t_1_pace_nor','Future_time']

    final_dataset['Av_Rank_nor^2'] = final_dataset['Av_Rank_nor']**2
    final_dataset['Av_Rank_nor^3'] = final_dataset['Av_Rank_nor']**3
    final_dataset['Av_Rank_nor^4'] = final_dataset['Av_Rank_nor']**4
    final_dataset['t_1_Rank_nor^2'] = final_dataset['t_1_Rank_nor']**2
    final_dataset['t_1_pace_nor^2'] = final_dataset['t_1_pace_nor']**2
    final_dataset['t_1_pace_nor^3'] = final_dataset['t_1_pace_nor']**3
    final_dataset['t_1_pace_nor^4'] = final_dataset['t_1_pace_nor']**4
    final_dataset['Age_nor^2'] = final_dataset['Age_nor']**2

    final_dataset['column1'] = 1


    return final_dataset


#-----CLOSED FORM SOLUTION-----
def ClosedForm ( x_feature,y_label,factor_lambda):
    ##--Multiply the matrices X^T by X and X^T by Y
    xt_x = np.dot(x_feature.transpose(),x_feature)
    xt_y = np.dot(x_feature.transpose(),y_label)

    matrix_lambda = factor_lambda*np.identity(len(xt_x))
    w_ridge = np.dot(np.linalg.inv(xt_x + matrix_lambda ),xt_y)
    return w_ridge

    #----CLOSED FORM SOLUTION METHOD FOR LINEAR REGRESSION----
def LinReg_diff_lambda (x_feature_cols,y_label, lambda_list, max_time, min_time):
    print('Initializing the Linear Regression...')


    #Dictionaries that will keep record of the mse values
    mse_dictionary_train={}
    mse_dictionary_test={}

    ##--tunning the lambda factor 
    print('Working on different lambdas...')
    for factor_lambda in lambda_list:
    ##--lists that will keep record of the mse values and later put it into the dictiontary        
        mse_list_train = []
        mse_list_test = []

        i=1
        ##--Generate 5 splits in the data set, in which each one : 70% corresponds to training data and 30% to test
        #shuffle_train_test = ShuffleSplit(n_splits = 5, test_size = 0.3, random_state = 0)
        for i in range(5):
            print('Running Cross Validation: iteration = ', i,' for lambda = ',factor_lambda)
            i+=1
            ##random value to randomize the choice of the test and training data -- 5 times
            x_train,x_test,y_train,y_test = train_test_split(x_feature_cols,y_label,random_state=random.randint(0,100))

            ## -- calls the function that to the linear regression
            w_ridge = ClosedForm ( x_train,y_train,factor_lambda)

            ##--Calculating the mse
            
            print('predicting times for the training set')
            x_train['Prediction'] = 0
            for row in range(len(x_train.index)):
                row_dataFrame = np.array(x_train.iloc[row,:-1])
                x_train.iloc [row,-1] = np.dot(row_dataFrame, w_ridge )*(max_time-min_time) + min_time #de-normalizing the prediction times
            x_train['Error^2'] = ((y_train*(max_time-min_time) + min_time) - x_train['Prediction'])**2
    
            print('predicting times for the test set')
            x_test['Prediction'] = 0
            for row in range(len(x_test.index)):
                row_dataFrame = np.array(x_test.iloc[row,:-1])
                x_test.iloc [row,-1] = np.dot(row_dataFrame, w_ridge)*(max_time-min_time) + min_time #de-normalizing the prediction times
            x_test['Error^2'] = ((y_test*(max_time-min_time) + min_time) - x_test['Prediction'])**2

            print(x_train.head())
            mse_train = (np.sum( x_train['Error^2'])/ len(x_train.index))
            mse_test = (np.sum( x_test['Error^2']) / len(x_test.index))
            
            mse_list_train.append(mse_train)
            mse_list_test.append(mse_test)
        mse_dictionary_train[ factor_lambda] = mse_list_train
        mse_dictionary_test[ factor_lambda] = mse_list_test
    
    new_mse_lambda_train={}
    new_mse_lambda_test={}

    for factor_lambda in lambda_list:
        new_mse_lambda_train[factor_lambda] = np.mean(mse_dictionary_train[factor_lambda])
        new_mse_lambda_test[factor_lambda] = np.mean(mse_dictionary_test[factor_lambda])

    with open('mean_square_error_training_set_non_normalized_time.txt','w') as output:
        output.write('training set\nlambda value\tmse')
        for key,value in new_mse_lambda_train.items():
            output.write(str(key)+'\t'+str(value)+'\n')
        output.write('\n\n\n')
        output.write('test set\nlambda value\tmse')
        for key,value in new_mse_lambda_test.items():
            output.write(str(key)+'\t'+str(value)+'\n')
    output.closed
    
    min_lambda = 100000000
    min_lambda = 0
    for factor_lambda, mse in mse_dictionary_test:
        if (min_lambda > mse):
            min_lambda = mse
            min_lambda = factor_lambda
    
    return factor_lambda



def main():
    dataFrame = pd.read_csv("recurrent_runners.csv")
    
    data = normalize_rank(dataFrame)
    
    max_time = max(np.array(data['Time']))
    min_time = min(np.array(data['Time']))


    data = normalize_other_fields(data)

    final_dataset = new_DataSet (data)

    ##sns.pairplot(final_dataset,x_vars=['Age_nor','Age_nor^2','Av_Rank_nor','Av_Rank_nor^2','Av_Rank_nor^3','Fastest_Pace_nor', 'Slowest_Pace_nor','t_1_pace_nor','t_1_pace_nor^2','t_1_pace_nor^3','column1'],y_vars=['Future_time_nor'])#'Gender',
    ##plt.show()

    #taking both features and the label we are going to train
    x_feature_cols = final_dataset[['Age_nor','participations','Gender','Av_Rank_nor','Av_Rank_nor^2','Av_Rank_nor^3', 't_1_pace_nor','t_1_pace_nor^2','t_1_pace_nor^3','column1']]#'Slowest_Pace_nor','t_1_pace_nor^4','Av_Rank_nor^4','Fastest_Pace_nor',
    y_label = final_dataset['Future_time']
    all_lambdas = [0 , 0.01 , 0.1, 0.25, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4 , 8 , 10, 20, 100 ]

    max_lambda = LinReg_diff_lambda (x_feature_cols,y_label, all_lambdas, max_time, min_time)

    final_weights = ClosedForm ( x_feature_cols, y_label, max_lambda)
    
    print(final_weights)


    #MISSING THE FINAL PREDICTION
    

if __name__ =='__main__':
    main()