Ultra short How To Git (For Windows) 

Open commandprompt and ensure you are on the right path

1. git status
See a list of files you have changed (ie. HowToGit.txt) in red 

2. git add filename
Choose files you will add (ie. HowToGit.txt)

3. git status
The files you have added should now appear in green (If not do step 2 again) 

4. git commit -m "What did I change in the files"
You now put all files you have changed into one package. 

5. git push 
Type in password when prompted
Sends the package to online repo for everybody to fetch. 
If you get an errror message do the following 
	5a. git pull 
	5b. git push 

Always do git-pull before you start working on the files to ensure you have the newest version